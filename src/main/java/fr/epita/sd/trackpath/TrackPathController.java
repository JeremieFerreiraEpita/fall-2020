package fr.epita.sd.trackpath;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api")
public class TrackPathController {
	@Autowired
	private TrackPathRepository repository;
	
	@RequestMapping(value="tracks", method = RequestMethod.GET, produces = MediaType.APPLICATION_ATOM_XML_VALUE)
	@ResponseBody
	private List<TrackPath> getAll() {
		return repository.findAll();
	}
	
	
	@RequestMapping(value="track/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_ATOM_XML_VALUE)
	@ResponseBody
	private TrackPath getById(@PathVariable("id") Long id) {
		return repository.findById(id).orElse(null);
	}
}
