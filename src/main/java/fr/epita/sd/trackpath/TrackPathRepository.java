package fr.epita.sd.trackpath;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TrackPathRepository extends JpaRepository<TrackPath, Long> {

}
