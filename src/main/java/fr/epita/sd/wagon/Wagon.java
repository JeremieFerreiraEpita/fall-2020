package fr.epita.sd.wagon;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Wagon {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(length = 78)
	private String name;
	
	private Integer netWeight;
	private Integer maxLoad;
	private Integer length;
	private Integer axlesCount;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getNetWeight() {
		return netWeight;
	}
	public void setNetWeight(Integer netWeight) {
		this.netWeight = netWeight;
	}
	public Integer getMaxLoad() {
		return maxLoad;
	}
	public void setMaxLoad(Integer maxLoad) {
		this.maxLoad = maxLoad;
	}
	public Integer getLength() {
		return length;
	}
	public void setLength(Integer length) {
		this.length = length;
	}
	public Integer getAxlesCount() {
		return axlesCount;
	}
	public void setAxlesCount(Integer axlesCount) {
		this.axlesCount = axlesCount;
	}
}
