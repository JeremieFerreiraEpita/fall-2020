package fr.epita.sd.wagon;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WagonRepository extends JpaRepository<Wagon, Long> {

}
