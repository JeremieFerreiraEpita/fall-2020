package fr.epita.sd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import fr.epita.sd.station.Station;
import fr.epita.sd.station.StationRepository;
import fr.epita.sd.trackpath.TrackPath;
import fr.epita.sd.trackpath.TrackPathRepository;
import fr.epita.sd.wagon.Wagon;
import fr.epita.sd.wagon.WagonRepository;

@Component
public class Initialization {

	private Logger logger = LoggerFactory.getLogger(Initialization.class);
	
	@Autowired
	private WagonRepository wagonRepository;
	
	@Autowired
	private StationRepository stationRepository;
	
	@Autowired
	private TrackPathRepository trackPathRepository;
	
	private static final String ATTRIBUTE_FORMAT = " +%s: %s";
	private static final String LINE_FORMAT = "Line #%d:";
	private static final String DELIMITOR = ";";
	private static final String FILENAME = "wagons.csv";
	
	@PostConstruct
	private void initialize() {
		
		List<TrackPath> tracks = trackPathRepository.findAll();
		
		logger.trace(tracks.toString());
		
		readJson();
		readXml();
		readCsv();
	}
	
	private String loadFileAsStr(String filepath) throws URISyntaxException, IOException {
		URI uri = Initialization.class.getClassLoader().getResource(filepath).toURI();
		return new String(Files.readAllBytes(Paths.get(uri)), StandardCharsets.UTF_8);
	}

	private void readXml() {
		try {
			// load the file
			String xml = loadFileAsStr("tracks.xml");
			
			// create the xml mapper
			XmlMapper mapper = new XmlMapper();

			// We can use new TypeReference<List<SomeClass>>(){} to map the result to a list
			List<TrackPath> tracks = mapper.readValue(xml, new TypeReference<List<TrackPath>>(){});
			
			logger.info("tracks.xml Successfully loaded.");
			logger.info("Number of records in tracks.xml: {}", tracks.size());
			
			// Save into database
			if (trackPathRepository.count() == 0) {
				trackPathRepository.saveAll(tracks);
			}
			
		} catch (IOException | URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	private void readJson() {
		try {
			// load the file
			String json = loadFileAsStr("stations.json");
			
			// initialize Gson
			Gson gson = new Gson();
			
			// call fromJson to deserialize json, we can use a new TypeToken to map the result to a list
			List<Station> stations = gson.fromJson(json, new TypeToken<ArrayList<Station>>(){}.getType());
			
			// save into database
			if (stationRepository.count() == 0) {
				stationRepository.saveAll(stations);
			}
			
			String serializedJson = gson.toJson(stations);
			
			logger.info("Stations.json successfully loaded!");
			logger.info("Number of records: {}", stations.size());
			logger.info("Serialized json: {}", serializedJson);
			
		} catch (URISyntaxException | IOException e) {
			e.printStackTrace();
		}
	}

	private void readCsv() {
		List<Wagon> wagons = new ArrayList<>();

		try {
			// create the file reader
			BufferedReader reader = new BufferedReader(new InputStreamReader(Main.class.getClassLoader().getResourceAsStream(FILENAME)));
			
			// read the first line to get attributes name
			String line = reader.readLine();
			String[] attributeNames = line.split(DELIMITOR);
			line = reader.readLine();
			
			// loop on each row
			int row = 1;
			while (line != null) {
				// display the line info
				System.out.println(String.format(LINE_FORMAT, row));
				
				// get the row values
				String[] values = line.split(DELIMITOR);
				
				Wagon wagon = new Wagon();
				wagon.setName(values[1]);
				wagon.setNetWeight(Integer.parseInt(values[2]));
				wagon.setMaxLoad(Integer.parseInt(values[3]));
				wagon.setLength(Integer.parseInt(values[4]));
				wagon.setAxlesCount(Integer.parseInt(values[5]));
				
				wagons.add(wagon);
				
				// loop on each attribute (or column)
				for (int col = 0; col < values.length; ++col) {
					// display attribute name and value for the current column
					System.out.println(String.format(ATTRIBUTE_FORMAT, attributeNames[col], values[col]));
				}
				
				// prepare next row
				System.out.println();
				++row;
				line = reader.readLine();
			}
			
			// save to database
			if (wagonRepository.count() == 0) {
				wagonRepository.saveAll(wagons);
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
